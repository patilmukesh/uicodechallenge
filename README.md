# UiCodeChallenge
This repository has automation test cases added for testing shopping cart sample application.
## How to Run tests

Please follow steps on running scripts(using terminal):
- Step 1: clone this repository on local using git clone "gitHttpURLOfRepo"
- Step 2: Open terminal and navigate to cloned project directory
- Step 3: Run command "npm install" in terminal
- Step 4: To test features, run below command
    - $ npm run test
