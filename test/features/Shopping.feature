Feature: This feature is to validate shopping website.
  @ShoppingE2E
  Scenario Outline: To validate user actions to sort items, add item, delete item in cart & finishing checkout.
    Given I open Swaglab login page
    When "<UserType>" login into website using user id and password assigned
    Then user verify 'Products' landing page is displayed
    When user selects "<SortOption>" to high sort option
    Then user verify all products displayed in "<SortOption>" order
    And user adds "<numberofItems>" to Cart by selecting 'Add to Cart' option
    And user verify all selected items are added in shopping cart page
    Then user remove "<ItemNumber>" item from shopping cart and selects continue shopping
    And user verify 'Products' landing page is displayed
    Then user selects "<SortOption>" to high sort option
    And user adds an item to Cart by selecting 'Add to Cart' option
    And user verify all selected items are added in shopping cart page
    And user navigate to Information page to submit by filling "<FirstName>", "<LastName>" and "<Zip>"
    And user verify item total price on Overview Page

    Examples:
      |UserType  |SortOption|numberofItems|ItemNumber|FirstName|LastName|Zip|
      |standarduser|lowtohigh|3           |1         |Test First|Test Last|53154|
