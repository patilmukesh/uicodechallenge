module.exports = {
  sortOptions: {
    AtoZ:'az',
    ZtoA:'za',
    lowtohigh: 'lohi',
    hightolow: 'hilo',
  }
};
