import * as assert from "assert";
const lookupValues = require('../utilities/lookupValues');
const util = require('../utilities/utilities');
let prodAddedCart=[];

export class ShoppingPage {
  get prodAddedCartArray() { return prodAddedCart;}
  get pageHeader() {return browser.$('//div[@class="product_label"]');}
  get productSortOption()  { return browser.$('//select[@class="product_sort_container"]');}
  get prodItemList() { return browser.$$('//div[@class="inventory_item"]');}
  get getEligibleAddProdList() {return browser.$$('//button[@class="btn_primary btn_inventory"]/ancestor::div[@class="inventory_item"]');}
  get shopCartContElem() {return browser.$("#shopping_cart_container");}
  get addToCartButton() {return 'button[class="btn_primary btn_inventory"]';}
  get itemNameXPath() {return 'div[class="inventory_item_name"]';}
  get itemPriceXPath() {return 'div[class="inventory_item_price"]';}

  verifyProductPage() {
    browser.waitUntil(()=>this.pageHeader.waitForDisplayed());
    expect(this.pageHeader).toHaveText('Products');
  }
  selectSortOption(sortOption) {
    const selectOption = this.productSortOption;
    selectOption.selectByAttribute('value',lookupValues.sortOptions[sortOption]);
  }
  verifySortedProduct(sortOption){
    browser.waitUntil(()=>this.pageHeader.waitForDisplayed());
    assert.equal(util.verifySortedProd(sortOption,this.prodItemList),'ascending','Products are displayed in ascending order');
  }
  addProductsToCart(numberofItems){
    const prodEligibleToAdd = this.getEligibleAddProdList;
    if(prodAddedCart.length ===0){
      prodAddedCart = new Array(numberofItems);
      for (let i = 0; i < numberofItems; i++) {
        prodAddedCart[i] = new Array(2);
        prodAddedCart[i][0] = util.getChildElementTextValue(prodEligibleToAdd[i],this.itemNameXPath);
        prodAddedCart[i][1] = util.getChildElementTextValue(prodEligibleToAdd[i],this.itemPriceXPath);
        util.getChildElement(prodEligibleToAdd[i],this.addToCartButton).click();
      }
    }else{
      for (let i = 0; i < numberofItems; i++) {
        prodAddedCart.push([util.getChildElementTextValue(prodEligibleToAdd[i],this.itemNameXPath),util.getChildElementTextValue(prodEligibleToAdd[i],this.itemPriceXPath)]);
        util.getChildElement(prodEligibleToAdd[i],this.addToCartButton).click();
      }
    }
    for (let i = 0; i < prodAddedCart.length; i++){
      console.log('Array elements added in cart are Name --->',prodAddedCart[i][0]);
      console.log('Array elements added in cart are Price --->',prodAddedCart[i][1]);
    }
  }
  navToCartPage() {
    this.shopCartContElem.click();
  }
  delElemFromArray(itemToDelName,itemToDelPrice) {
    let arrayIndex='';
    for (let i = 0; i < prodAddedCart.length; i++) {
      if(prodAddedCart[i][0] === itemToDelName && prodAddedCart[i][1] === itemToDelPrice){
        arrayIndex = i;
        break;
      }
    }
    prodAddedCart.splice(arrayIndex,1);
  }
}
const shoppingPage = new ShoppingPage();
export {shoppingPage};
